let { google } = require('googleapis');
let admob = google.admob('v1');

adMobData = await admob.accounts.mediationReport.generate({
            parent: parent,
            requestBody: {
                report_spec: {
                    date_range: {
                        start_date: { year: start_year, month: start_month, day: start_day },
                        end_date: { year: end_year, month: end_month, day: end_day }
                    },
                    dimensions: ["APP"],
                    metrics: [
                        "ESTIMATED_EARNINGS", "OBSERVED_ECPM", "AD_REQUESTS", "MATCH_RATE", "MATCHED_REQUESTS", "IMPRESSIONS", "IMPRESSION_CTR", "CLICKS"
                    ]
                    // sort_conditions: [{ dimension: "APP", order: "ASCENDING" }],
                    // timeZone: "America/Los_Angeles"
                    // localizationSettings: { currency_code: "USD", language_code: "en-US" }
                }
            }
        });
